package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.VideoGamesService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
public class VideoGamesServiceTest {

    @Autowired
    VideoGamesRepository videoGamesRepository;

    @Autowired
    VideoGamesService videoGamesService;

    @Autowired
    VideoGame videoGame;

    @Test
    void testOfAddingVideoGame(){

        Mockito.when(videoGame.getName()).thenReturn("test");
       Mockito.when(videoGame.getId()).thenReturn(1234L);
        videoGamesService.addVideoGame("test");
        assertEquals("test", videoGamesService.getOneVideoGame(1234L));

    }

    @TestConfiguration
    static class TestConfig {
        @Bean
        @Primary
        public VideoGame videoGame(){return Mockito.mock(VideoGame.class);}
        }
    }



